import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class TestDemo {
    @Test
    public void sampleTest() {
        assertThat(1, is(equalTo(1)));
    }
}
